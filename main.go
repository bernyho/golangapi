package main

import (
	"context"
	firebase "firebase.google.com/go"
	"fmt"
	"github.com/gin-gonic/gin"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
	"log"
	"net/http"
)

type Task struct {
	Name   string "json:name"
	Author string "json:author"
}

func GetTasks() ([]Task, error) {
	sa := option.WithCredentialsFile("credentials.json")
	app, err := firebase.NewApp(context.Background(), nil, sa)
	if err != nil {
		log.Fatalln(err)
	}
	client, err := app.Firestore(context.Background())
	if err != nil {
		log.Fatalln(err)
	}

	iter := client.Collection("sampleData").Documents(context.Background())

	results := make([]Task, 0)
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Failed to iterate: %v", err)
		}
		results = append(results, Task{
			Name:   fmt.Sprintf("%v", doc.Data()["Author"]),
			Author: fmt.Sprintf("%v", doc.Data()["Name"]),
		})
	}
	defer client.Close()

	return results, nil
}

func main() {
	/*
		GET    /lists                          # returns all lists for the user
		POST   /lists                          # creates a new list for a user
		GET    /lists/{listId}/tasks           # returns all tasks from the users' list
		POST   /lists/{listId}/tasks           # creates a new task in the users' list
		GET    /lists/{listId}/tasks/{taskId}  # returns only single task from the list (detail)
		PATCH  /lists/{listId}/tasks/{taskId}  # updates the task in the list
		DELETE /lists/{listId}/tasks/{taskId}  # deletes the task from the list
	*/
	r := gin.Default()

	r.GET("/lists", func(context *gin.Context) {
		// returns all lists for the user
		results, err := GetTasks()
		if err != nil {
			context.JSON(http.StatusInternalServerError, gin.H{"status": "internal error: " + err.Error()})
			return
		}
		context.JSON(http.StatusOK, results)
	})

	/*r.POST("/lists", func(context *gin.Context) {
		// creates a new list for a user
		context.JSON(http.StatusOK, task)
	})

	r.GET("/lists/:listId/tasks", func(context *gin.Context) {
		context.JSON(http.StatusOK, Task{"returns all tasks from the users' list"})
	})

	r.POST("/lists/:listId/tasks", func(context *gin.Context) {
		context.JSON(http.StatusOK, Task{"creates a new task in the users list"})
	})

	r.GET("/lists/:listId/tasks/:taskId", func(context *gin.Context) {
		context.JSON(http.StatusOK, Task{Type: "returns only single task from the list (detail)"})
	})

	r.PATCH("/lists/:listId/tasks/:taskId", func(context *gin.Context) {
		context.JSON(http.StatusOK, Task{Type: "updates the task in the list"})
	})

	r.DELETE("/lists/:listId/tasks/:taskId", func(context *gin.Context) {
		context.JSON(http.StatusOK, Task{Type: "deletes the task from the list"})
	})*/

	// running the http server
	log.Println("running..")
	if err := r.Run(":8080"); err != nil {
		panic(err)
	}
}
